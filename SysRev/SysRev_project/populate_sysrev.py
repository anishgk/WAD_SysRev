import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'SysRev_project.settings')

import django
django.setup()

from SysRev.models import Review, Paper
from django.contrib.auth.models import User

def populate():

    cancer_rev = add_review('Cancer','descriptive text', 2016-6-1, "cancer", 4000, 1000, 20)

    add_paper(cancer_rev, 'Dealing with the big C', 'Dr Seuss', 'text for an abstract', 2009-3-13, 'url@url.url', True, True, 'non')

    add_paper(cancer_rev, 'Cancer by Mahoney', 'Dr Mahoney', 'another abstract', 2000-9-14, 'diffurl.url', True, False, 'notes')

    add_paper(cancer_rev, 'Chicken can help', 'Dr Nandos', 'third abstract', 1995-2-1, 'thirdurl.url', False, False, 'Peri Peri it')

    blood_rev = add_review('Blood', 'descriptive text on blood', 2016-6-5, "blood", 1000, 5000, 10)

    add_paper(blood_rev,'Chicha4Pres', 'Dr Chicharito', 'bloody abstract', 1996-6-6, 'bloodyurl.url', True, True, 'football')

    add_paper(blood_rev,'Another one on blood', 'Dr Khaled', 'They dont want us to bleed', 1945-2-7, 'khaledurl.url', False, False, 'bless up')

    add_paper(blood_rev,'The Life', 'Dr West', 'No more blood in LA', 2005-10-24, 'westblood.url', True, False, 'please')

    add_user("jill","jill@jill.com","jill")
    add_user("bob","bob@bob.com", "bob")
    add_user("jen","jen@jen.com","jen")

    for r in Review.objects.all():
        for p in Paper.objects.filter(review=r):
            print"- {0} - {1}".format(str(r), str(p))

            
def add_review(title, description, date_started, query_string, pool_size, abstracts_judged, document_judged):
    r = Review.objects.get_or_create(title = title)[0]
    r.description = description
    r.date_started = date_started
    r.query_string = query_string
    r.pool_size = pool_size
    r.abstracts_judged = abstracts_judged
    r.document_judged = document_judged
    return r

def add_paper(review, title, authors, abstract, publish_date, paper_url, abstract_relevance, document_relevance, notes):
    p = Paper.objects.get_or_create(review=review, title=title)[0]
    p.authors=authors
    p.abstract=abstract
    p.publish_date = publish_date
    p.paper_url = paper_url
    p.abstract_relevance=abstract_relevance
    p.document_relevace=document_relevance
    p.notes=notes
    return p

def add_user(username, email, password):
    u = User.objects.get_or_create(username=username,
                                   email=email,
                                   password=password)[0]
    return u

if __name__ == '__main__':
    print "Starting population script"
    populate()
