from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Researcher(models.Model):	 
    user = models.OneToOneField(User)

    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __unicode__(self):	 
        return self.user.username


class Review(models.Model):	 
    title = models.CharField(max_length=128)	 
    description = models.TextField()	 
    date_started = models.DateField(null = True, blank = True)	 
    query_string = models.TextField()	 
    pool_size = models.IntegerField(default=0)	 
    abstracts_judged = models.IntegerField(default=0)	 
    document_judged = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title


class Paper(models.Model):	 
    review = models.TextField()	 
    title = models.CharField(max_length=128)	 
    authors = models.TextField()	 
    abstract = models.TextField()	 
    publish_date = models.DateField(null = True, blank = True)	 
    paper_url = models.URLField()
    abstract_relevance = models.BooleanField(default=None)
    document_relevance = models.BooleanField(default=None)
    notes = models.CharField(max_length=128)
    
    def __unicode__(self):
        return self.title


class Query(models.Model):
    review = models.ForeignKey(Review)

    def __unicode__(self):
        return self.title

class Favourite(models.Model):
    title = models.CharField(max_length=128)
    paper_url = models.URLField()
