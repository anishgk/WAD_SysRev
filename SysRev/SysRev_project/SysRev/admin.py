from django.contrib import admin
from SysRev.models import Researcher, Review, Paper, Query

admin.site.register(Researcher)
admin.site.register(Review)
admin.site.register(Paper)
admin.site.register(Query)
