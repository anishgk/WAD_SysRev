from django import forms
from django.contrib.auth.models import User
from SysRev.models import Researcher, Review, Paper, Query


#class PaperForm(forms.ModelForm):
#    class Meta:
#        model = Paper
#
#    def clean(self):
#        cleaned_data = self.cleaned_data
#        url = cleaned_data.get('url')
#
#        # If url is not empty and doesn't start with 'http://', prepend 'http://'.
#        if url and not url.startswith('http://'):
#            url = 'http://' + url
#            cleaned_data['url'] = url
#
#        return cleaned_data


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Researcher
        fields = ('website', 'picture')
