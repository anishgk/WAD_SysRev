# -*- coding: utf-8 -*-

from Bio import Entrez, Medline
from urllib2 import HTTPError
import json, ast , datetime, time, sys, urllib, urllib2

reload(sys)
sys.setdefaultencoding('utf-8')

def run_query(search_terms):
    
    Entrez.email = "leif.azzopardi@glasgow.ac.uk"
    search_results = Entrez.read(Entrez.esearch(db="pubmed",
                                                term= "search_terms",
                                                reldate=365, datetype="pdat",
                                                usehistory="y"))
    
    count = int(search_results["Count"])
    batch_size = 10
    for start in range(0,count,batch_size):        
        attempt = 1
        while attempt <= 3:

            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",
                                             retmode="xml",retstart=start,
                                             retmax=batch_size,
                                             webenv=search_results["WebEnv"],
                                             query_key=search_results["QueryKey"])
                attempt += 1

            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    attempt += 1
                    time.sleep(15)
                else:
                    raise

        def toDate(dateDictionary):
            if len(dateDictionary) != 0:
                dateDictionary = dateDictionary[0]
                day = int(dateDictionary['Day'])
                month = int(dateDictionary['Month'])
                year = int(dateDictionary['Year'])
                date = datetime.date(year,month,day)
                
            else:
                date = datetime.date(2999,12,31)
            return date     
            
        def getAffiliation(author):
            if 'AffiliationInfo' in author.keys():
                if len(author['AffiliationInfo']) != 0:
                    affiliation = author['AffiliationInfo'][0]['Affiliation']
                    return affiliation
                else:
                    return "No institution found"
            else:
                return "No institution found"


        def getAbstract(resultDictionary):
            if 'Abstract' in resultDictionary.keys():
                if len(resultDictionary['Abstract']) != 0:
                    abstract = resultDictionary['Abstract']['AbstractText']
                    return abstract
                else:
                    return "No abstract found"
            else:
                return "No abstract found"

        def getName(author):
            if 'ForeName'in author.keys():
                forename = author['ForeName']
            else:
                forename = "No forename found"

            if 'LastName' in author.keys():
                lastname = author['LastName']
            else:
                lastname = "No lastname found"

            name = forename+" "+lastname
            return name

        def getAuthors(article):
            if 'AuthorList' in article.keys():
                authors = article['AuthorList']
                
            else:
                authors = []

            return authors
        
    data = Entrez.read(fetch_handle)
    
    result_list = []
    for record in data:
        
        ID = record(['MedlineCitation']['PMID'])

        title = record(['MedlineCitation']['Article']['ArticleTitle'])

        authors = getAuthors(record['MedlineCitation']['Article'])
        authors_list = []
        authorsString = ""

        if len(authors) != 0:
            for author in authors:
                name = getName(author)
                affiliation = getAffiliation(author)
                info = name+"-"+affiliation
                authors_list.append(info)

            for item in authors_list:
                authorsString += "●"+item
        else:
            authorsString = "No authors found"
        
        pubDate = toDate(record['MedlineCitation']['Article']['ArticleDate'])
        abstract = getAbstract(record['MedlineCitation']['Article'])
        abstract_formatted = ""
        for citation in abstract:
            abstract_formatted = abstract_formated + abstract[citation]

        link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&id=%s&cmd=prlinks&retmode=ref%id="%(ID)

        result_list.append({
            'id':ID,
            'title':title,
            'authors':authorsString,
            'date':pubDate,
            'abstract':abstract_formatted,
            'link':link
            })

        
    fetch_handle.close()
    return result_list

