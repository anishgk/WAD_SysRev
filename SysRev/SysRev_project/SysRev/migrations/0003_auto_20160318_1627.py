# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SysRev', '0002_auto_20160316_0016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paper',
            name='publish_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='review',
            name='date_started',
            field=models.DateField(null=True, blank=True),
        ),
    ]
