# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SysRev', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paper',
            name='abstract_relevance',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='paper',
            name='document_relevance',
            field=models.BooleanField(default=False),
        ),
    ]
