from django.conf.urls import patterns, url
from SysRev import views
from django.contrib.auth import views as auth_views
from django.core.urlresolvers import reverse_lazy

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^howto/$', views.howto, name='howto'),
        url(r'^search/$', views.search, name='search'),
        url(r'^favourite/$', views.favourite, name='favourite'),
        url(r'^password/change/$', auth_views.password_change,
            {'post_change_redirect': reverse_lazy('auth_password_change_done')},
            name='auth_password_change'),
        url(r'^password/change/done/$',
            auth_views.password_change_done,
            name='auth_password_change_done'),
        )
