from django.shortcuts import render
from django.http import HttpResponse
from SysRev.models import Researcher, Review, Paper, Query
from SysRev.forms import UserForm, UserProfileForm
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from datetime import datetime
from SysRev.search import run_query
# Create your views here.


def index(request):
    context_dict = {}
    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).seconds > 0:
            # ...reassign the value of the cookie to +1 of what it was before...
            visits = visits + 1
            # ...and update the last visit cookie, too.
            reset_last_visit_time = True
    else:
        # Cookie last_visit doesn't exist, so create it to the current date/time.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits
    context_dict['visits'] = visits

    return render(request, 'SysRev/index.html', context_dict)


def howto(request):
    context_dict = {}
    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).seconds > 0:
            # ...reassign the value of the cookie to +1 of what it was before...
            visits = visits + 1
            # ...and update the last visit cookie, too.
            reset_last_visit_time = True
    else:
        # Cookie last_visit doesn't exist, so create it to the current date/time.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits
    context_dict['visits'] = visits

    return render(request, 'SysRev/howto.html', context_dict)

def search(request):

    result_list = []

    if request.method == 'POST':
        search_terms = request.POST['search_terms']

        if search_terms:
            # Run API Function to get list of abstracts
            result_list = run_query(search_terms)

    return render(request, 'SysRev/search.html', {'result_list': result_list})

def favourite(request):
    context_dict = {}
    try:
        favourite = Favourite.objects.get(title)
        context_dict['favourite_title'] = favourite.title
        context_dict['favourite_url'] = favourite.url
        context_dict['favourite'] = favourite
    except Favourite.DoesNotExist:
        pass

    return render(request, 'SysRev/fav.html', context_dict)



