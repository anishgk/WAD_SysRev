﻿# Systematic Review Application

*Please not that homepage URL is case-sentive --> /SysRev/
  
  
The API to use is provided by E-Utilities which gives access to the PubMed api:

  - http://www.ncbi.nlm.nih.gov/home/api.shtml


* Friday, 4th March 2016
	-First commit
	-Added 'SysRev' app to project
	-Added urls.py to SysRev app
	-Updated urls.py in SysRev_project to include urlpatterns from app
	-Added Static and Media file paths to project's settings.py
	-Added templates folder
	-Added test index.html
	-Added index() in views.py
	-templates/SysRev.urls.py done (ALMO)

* Wednesday, 9th March 2016
	-Authentication set up (ALMO)
	-Added models to SysRev/models.py

* Thursday, 10th March 2016
	-Models fixed (ALMO)
	-Added forms to SysRev/forms.py (ALMO)
	§ Need to start views.py

* Monday, 14th March 2016
	-views.py made (ALMO)
	§ Problem with models.researcher on ImageField (which is not required anyways)

* Tuesday, 15th March 2016
	-Commented out authentication code from SysRev/settings.py
	-Fixed context dictionary issues
	-Fixed templates (links now work)
	-Deleted "<li><a href="{% url 'auth_login' %}">Login/Register</a></li>" from base.html since {% %} cant be commented out (to paste again once authentication is set up)
	
* Wednesday, 16th March 2016
	-Database has been updated (default of Boolean values in models.py set to False) and is now up
	-PaperForm added to Forms.py (not sure if needed) with self_clean for cleaner urls (commented out)
	§ Git commit error has caused our code to copy itself and dump "HEAD" and a 13 digit commit code randomly into our files
	-Added registration to INSTALLED_APPS in SysRev/settings.py
	-Added django-registration-redux 1.4 to requirements.txt

* Friday, 18th March 2016
	-Population script created with users (ALMO)

* Saturday, 19th March 2016
	-Registration redux up (ALMO)

GitLab Accounts:
-------------------------
- almok - Almohannad Albastaki 2073320
- anishgk - Anish Kenkare 2144482
- anzhelaGu - Anzhela Ivanova 2144620
- prateekewl - Prateek Gajwani 2137368

Instructions:
-------------------------
-To git clone:
	https://<USERNAME>:<PASSWORD>@gitlab.com/anishgk/WAD_SysRev.git
-Virtual environment name = rango
- Carry out the following pip installs for ve
	pip install -U Django==1.7
	pip install pillow
	pip install biopython
	pip install django-registration-redux
-Only one population script present -- populate_sysrev.py
	user names in population script are jill, bob jen with password as their names
